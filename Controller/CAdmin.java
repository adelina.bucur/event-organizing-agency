package Controller;

import Model.User;
import Model.UserPersistence;
import View.AdminView;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class CAdmin {
    public AdminView adminView;
    public User user;
    public CAdmin(AdminView adminView) {
        this.adminView = adminView;
        this.adminView.getAddUserButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("lala");
                UserPersistence userPersistence = null;
            try {
                userPersistence = new UserPersistence();
                User user = new User();
                System.out.println(adminView.getPasswordField().getText().toString());
                userPersistence.addUser(new User(adminView.getFirstNameField().getText().toString(), adminView.getLastNameField().getText().toString(),  adminView.getUsernameField().getText().toString(),adminView.getPasswordField().getText().toString(),adminView.getComboBox().getSelectedItem().toString(), adminView.getEmailField().getText().toString() ));
                System.out.println("ok");
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }

        }

        });

        this.adminView.getUpdateUserButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserPersistence userPersistence = null;
                try {
                    userPersistence = new UserPersistence();
                    User user = new User();
                    userPersistence.updateUser(new User(adminView.getFirstNameField().getText().toString(), adminView.getLastNameField().getText().toString(), adminView.getUpdateUserButton().getText().toString(), adminView.getPasswordField().getText().toString(), adminView.getComboBox().getSelectedItem().toString(), adminView.getEmailField().getText().toString()),Integer.parseInt(adminView.getIdField().getText().toString()));
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            }

        });

        this.adminView.getDeleteUserButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserPersistence userPersistence = null;
                try {
                    userPersistence = new UserPersistence();
                    User user = new User();
                    userPersistence.userDelete(Integer.parseInt(adminView.getIdField().getText().toString()));
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }

            }
        });

        this.adminView.getViewAllUsersButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserPersistence userPersistence = null;
                try {
                    userPersistence = new UserPersistence();
                    java.util.List<User> list = userPersistence.selectAll();
                    String text = "";
                    for(User user: list) {
                        text += "-> " + user.getId() + " " + user.getUsername() + " " + user.getFirst_name() + " " + user.getLast_name() + " " + user.getRole() + "\n";
                    }
                    adminView.getTextArea1().setText(text);
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            }


        });

    }

}
