package Controller;

import Model.Event;
import Model.EventPersistence;
import Model.User;
import Model.UserPersistence;
import View.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

public class CLogin {
    private LoginView loginView;

    public CLogin(LoginView loginView) {
        this.loginView = loginView;

        loginView.getLoginButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UserPersistence userPersistence = null;
                try {
                    userPersistence = new UserPersistence();

                    String username = loginView.getUsernameField().getText().toString();
                    String password = new String(loginView.getPasswordField().getPassword());

                    User user = userPersistence.findByUsernameAndPassword(username, password);
                    if(user != null){
                        System.out.println("role:"+ user.getRole());
                        System.out.println("username:"+ user.getUsername());
                        if(user.getRole().equals("admin") ){
                            System.out.println("admin" );
                            AdminView adminView = new AdminView();
                            CAdmin cAdmin = new CAdmin(adminView);
                            adminView.setVisible(true);
                        } else if(user.getRole().equals("organizer") ){
                            System.out.println("organizer" );
                            OrganizerView organizerView = new OrganizerView ();
                            organizerView .setVisible(true);
                        } else if(user.getRole().equals("client") ){
                            System.out.println("client intrat" );
                            ClientView clientView = new ClientView ();
                            clientView .setVisible(true);
                        }
                    }

                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        loginView.getViewEventListButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                EventPersistence eventPersistence = null;
                try {
                    eventPersistence = new EventPersistence();
                    List<Event> list = eventPersistence.selectAll();
                    String text = "";
                    for(Event event: list) {
                        text += "-> " + event.getName() + " " + event.getLocation() + " " + event.getNrPersons() + " " + event.getDate()  + "\n";
                    }
                    loginView.getTextArea1().setText(text); ;
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        loginView.getRegisterButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RegisterView registerView = new RegisterView();
                registerView .setVisible(true);
            }
        });
    }

}
