package Controller;

import Model.User;
import Model.UserPersistence;
import View.AdminView;
import View.ClientView;
import View.OrganizerView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class COrganizer {
    public OrganizerView organizerView;
    public User user;
    public COrganizer(OrganizerView organizerView) {
        this.organizerView= organizerView;
        this.organizerView.getAddUserButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("lala");
                UserPersistence userPersistence = null;
                try {
                    userPersistence = new UserPersistence();
                    User user = new User();
                    System.out.println(organizerView.getPasswordField().getText().toString());
                    userPersistence.addUser(new User(organizerView.getFirstNameField().getText().toString(), organizerView.getLastNameField().getText().toString(),  organizerView.getUsernameField().getText().toString(),organizerView.getPasswordField().getText().toString(),organizerView.getComboBox().getSelectedItem().toString(), organizerView.getEmailField().getText().toString() ));
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }

            }

        });

        this.organizerView.getUpdateUserButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserPersistence userPersistence = null;
                try {
                    userPersistence = new UserPersistence();
                    User user = new User();
                    userPersistence.updateUser(new User(organizerView.getFirstNameField().getText().toString(), organizerView.getLastNameField().getText().toString(), organizerView.getUpdateUserButton().getText().toString(), organizerView.getPasswordField().getText().toString(), organizerView.getComboBox().getSelectedItem().toString(), organizerView.getEmailField().getText().toString()),Integer.parseInt(organizerView.getIdField().getText().toString()));
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            }

        });

        this.organizerView.getDeleteUserButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserPersistence userPersistence = null;
                try {
                    userPersistence = new UserPersistence();
                    User user = new User();
                    userPersistence.userDelete(Integer.parseInt(organizerView.getIdField().getText().toString()));
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }

            }
        });

        this.organizerView.getViewAllUsersButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserPersistence userPersistence = null;
                try {
                    userPersistence = new UserPersistence();
                    java.util.List<User> list = userPersistence.selectAll();
                    String text = "";
                    for(User user: list) {
                        text += "-> " + user.getId() + " " + user.getUsername() + " " + user.getFirst_name() + " " + user.getLast_name() + " " + user.getRole() + "\n";
                    }
                    organizerView.getTextArea1().setText(text);
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            }


        });

    }
}
