package Controller;

import Model.Event;
import Model.EventPersistence;
import Model.User;
import Model.UserPersistence;
import View.AdminView;
import View.ClientView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

public class ClientController {
    public ClientView clientView;
    public User user;
    public ClientController(ClientView clientView) {
        this.clientView = clientView;

        this.clientView.getEventListButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EventPersistence eventPersistence = null;
                try {
                    eventPersistence = new EventPersistence();
                    List<Event> list = eventPersistence.selectAll();
                    String text = "";
                    for(Event event: list) {
                        text += "-> " + event.getName() + " " + event.getLocation() + " " + event.getNrPersons() + " " + event.getDate()  + "\n";
                    }
                    clientView.getTextArea1().setText(text); ;
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            }


        });

    }

}
