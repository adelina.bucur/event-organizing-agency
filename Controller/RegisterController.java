package Controller;

import Model.User;
import Model.UserPersistence;
import View.RegisterView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.*;

public class RegisterController {

    private RegisterView registerView;
    private User user;

    public RegisterController(RegisterView view) {

        this.registerView = registerView;

        this.registerView.getRegisterButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("lala");
                UserPersistence userPersistence = null;
                try {
                    userPersistence = new UserPersistence();
                    User user = new User();
                    System.out.println(registerView.getPasswordField().getText().toString());
                    userPersistence.addUser(new User(registerView.getFirstNameField().getText().toString(), registerView.getLastNameField().getText().toString(), registerView.getUsernameField().getText().toString(), registerView.getPasswordField().getText().toString(), registerView.getComboBox().getSelectedItem().toString(), registerView.getEmailField().getText().toString()));
                    System.out.println("ok");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }

            }

        });


    }
}
