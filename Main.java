import Controller.CLogin;
import View.LoginView;

public class Main {
    public static void main(String[] args) {
        LoginView loginView = new LoginView();
        loginView.setVisible(true);
        CLogin clogin = new CLogin(loginView);
    }
}