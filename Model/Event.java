package Model;

import java.util.Date;

public class Event {
    public int idEvent;
    private Date date;
    private int idUser;
    private String location;
    private String name;
    private int nrPersons;
    private String scope;

    public Event(String text, int idUser, String text1, String text2, int nrPersons, String text3) {
    }

    public int getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    public Event() {
    }

    public Event(int idEvent, Date date, int idUser, String location, String name, int nrPersons, String scope) {
        this.idEvent = idEvent;
        this.date = date;
        this.idUser = idUser;
        this.location = location;
        this.name = name;
        this.nrPersons = nrPersons;
        this.scope = scope;
    }

    public Event(Date date, int idUser, String location, String name, int nrPersons, String scope) {
        this.date = date;
        this.idUser = idUser;
        this.location = location;
        this.name = name;
        this.nrPersons = nrPersons;
        this.scope = scope;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getNrPersons() {
        return nrPersons;
    }

    public void setNrPersons(int nrPersons) {
        this.nrPersons = nrPersons;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }



}
