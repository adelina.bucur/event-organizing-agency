package Model;

import Model.Event;
import Model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventPersistence {

    private Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/firmaevenimente", "root", "root");

    public EventPersistence() throws SQLException {
    }

    public void addEvent(Event event)  throws SQLException {

        try {

            Statement stmt = conn.createStatement();
            if(conn.isValid(5)){
                System.out.println("Connection done");
            }
            String sql = "INSERT INTO event ( date, id_user, location, name, nrpersons, scope) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            // statement.setDate(1, event.getDate());
            // statement.setTimestamp(1, new Timestamp(event.getDate().getTime()));
            statement.setDate(1, new java.sql.Date(event.getDate().getTime()));
            statement.setInt(2, event.getIdUser());
            statement.setString(3, event.getLocation());
            statement.setString(4, event.getName());
            statement.setInt(5, event.getNrPersons());
            statement.setString(6, event.getScope());

            statement.executeUpdate();

        } catch (SQLException ex1) {
            ex1.printStackTrace();
        }

    }

    public void updateEvent(Event event, int id) {
        try {
            Statement stmt = conn.createStatement();
            if(conn.isValid(5)){
                System.out.println("Connection done");
            }
            String sql = "UPDATE event SET date= ?, id_user = ?, location = ?, name = ?, nrpersons= ?, scope = ? WHERE id_event = ?";
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setTimestamp(1, new Timestamp(event.getDate().getTime()));
            statement.setInt(2, event.getIdUser());
            statement.setString(3, event.getLocation());
            statement.setString(4, event.getName());
            statement.setInt(5, event.getNrPersons());
            statement.setString(6, event.getScope());
            statement.setInt(7, id);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void eventDelete(int id) throws SQLException {
        String sql = "DELETE From event Where id_event = ?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);

        statement.executeUpdate();

    }

    public List<Event> selectAll() throws SQLException {
        String sql = "Select * from event";
        PreparedStatement statement = conn.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        List<Event> list = new ArrayList<>();

        while (result.next()) {

            int id_event = result.getInt("id_event");
            Date date = result.getTimestamp("date");
            int id_user = result.getInt("idUser");
            String location = result.getString("location");
            String nume = result.getString("nume");
            int nrPersons = result.getInt("nrpersons");
            String scope = result.getString("scope");

            Event event = new Event(id_event,date,id_user,location,nume,nrPersons,scope);
            list.add(event);

        }
        result.close();
        statement.close();
        return list;
    }

    public Event selectById(int id) throws SQLException {

        String sql = "Select * from event  Where id_event = ?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();

        Event event = new Event();

        if (result.next()) {
            //id = result.getInt(id);
            int id_event = result.getInt("id_event");
            Date date = result.getTimestamp("date");
            int id_user = result.getInt("id_user");
            String location = result.getString("location");
            String name = result.getString("name");
            int nrPersons = result.getInt("nrpersons");
            String scope = result.getString("scope");

            System.out.println("Event ID: " + id);
            System.out.println("Date: " + date);
            System.out.println("ID User: " + id_user);
            System.out.println("Location: " + location);
            System.out.println("Name: " + name);
            System.out.println("Nr Persons: " + nrPersons);
            System.out.println("Scope: " + scope);

        }else {
            System.out.println("User not found for id: " + id);
        }
        result.close();
        statement.close();


        return event;
    }

    public List<Event> filtredByScope() throws SQLException {
        String sql = "SELECT * FROM event WHERE id_event = ? ORDER BY scope = ?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, 1); // set the value of the first parameter to 1
        statement.setString(2, "someScope"); // set the value of the second parameter to "someScope"
        ResultSet result = statement.executeQuery();

        List<Event> list = new ArrayList<>();
        while (result.next()) {
            int id_event = result.getInt("id_event");
            Date date = result.getTimestamp("date");
            int id_user = result.getInt("id_user");
            String location = result.getString("location");
            String nume = result.getString("name");
            int nrPersons = result.getInt("nrpersons");
            String scope = result.getString("scope");

            Event event = new Event(id_event, date, id_user, location, nume, nrPersons, scope);
            list.add(event);
        }

        result.close();
        statement.close();
        return list;
    }

    public void deleteEvent(Event event) {
    }


    public int getId() {
        return getId();
    }
}
