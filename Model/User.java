package Model;

public class User {
    private Integer id;
    private String first_name;
    private String last_name;
    private String username;
    private String password;
    private String role;
    private String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }



//    public User(String first_name, String last_name, String username, String role, String password ) {
//        this.first_name = first_name;
//        this.last_name = last_name;
//        this.username = username;
//        this.password = password;
//        this.role = role;
//    }

    public User() {
    }

    public User(String first_name, String last_name, String username, String password, String role, String email) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.password = password;
        this.role = role;
        this.email = email;
    }

    public User(Integer id, String first_name, String last_name, String username, String password, String role, String email) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.password = password;
        this.role = role;
        this.email = email;
    }

    public User(Integer id, String first_name, String last_name, String password, String role, String username){
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

}