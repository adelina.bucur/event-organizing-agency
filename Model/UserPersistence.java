package Model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserPersistence {
    private Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/firmaevenimente", "root", "root");
    public UserPersistence() throws SQLException {
    }
    public void addUser(User user) throws SQLException {
        try {

            Statement stmt = conn.createStatement();
            if(conn.isValid(5)){
                System.out.println("Connection done");
            }
            String sql = "INSERT INTO user (FirstName, LastName, Email, password, Role, Username) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);


            System.out.println(user.getPassword());

            statement.setString(1, user.getFirst_name());
            statement.setString(2, user.getLast_name());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.setString(5, user.getRole());
            statement.setString(6, user.getUsername());

            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public void updateUser(User user, int id) {
        try {
            Statement stmt = conn.createStatement();
            if(conn.isValid(5)){
                System.out.println("Connection done");
            }
            String sql = "UPDATE user SET FirstName = ?, LastName = ?, Password = ?, Role = ?, Username = ? WHERE idUser = ?";
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setString(1, user.getFirst_name());
            statement.setString(2, user.getLast_name());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getRole());
            statement.setString(5, user.getUsername());
            statement.setInt(6, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void userDelete(int id) throws SQLException {
        String sql = "DELETE From user Where idUser = ?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);

        statement.executeUpdate();

    }

    public List<User> selectAll() throws SQLException {
        String sql = "Select * from user";
        PreparedStatement statement = conn.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        List<User> list = new ArrayList<>();

        while (result.next()) {
            int id = result.getInt("idUser");
            String firstName = result.getString("FirstName");
            String lastName = result.getString("LastName");
            String password = result.getString("Password");
            String role = result.getString("Role");
            String userName = result.getString("Username");

            User user1 = new User(id, firstName, lastName,password,role,userName);
            list.add(user1);

        }
        result.close();
        statement.close();
        return list;
    }

    public User selectById(int id) throws SQLException {

        String sql = "Select * from user  Where id = ?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();

        User user2 = new User();

        if (result.next()) {
            //id = result.getInt(id);
            String firstName = result.getString("first_name");
            String lastName = result.getString("last_name");
            String password = result.getString("password");
            String role = result.getString("role");
            String userName = result.getString("user_name");

            System.out.println("User ID: " + id);
            System.out.println("First Name: " + firstName);
            System.out.println("Last Name: " + lastName);
            System.out.println("Password: " + password);
            System.out.println("Role: " + role);
            System.out.println("User Name: " + userName);

        }else {
            System.out.println("User not found for id: " + id);
        }
        result.close();
        statement.close();


        return user2;
    }

    public User findByUsernameAndPassword(String username, String password) throws SQLException {
        String sql = "Select * from user  Where Username = ? and Password = ? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, username);
        statement.setString(2, password);
        ResultSet result = statement.executeQuery();

        User user = new User();

        if (result.next()) {
            int id = result.getInt("IdUser");
            String firstName = result.getString("FirstName");
            String lastName = result.getString("LastName");
            String password1 = result.getString("Password");
            String role = result.getString("Role");
            String userName = result.getString("Username");

            user.setId(id);
            user.setFirst_name(firstName);
            user.setLast_name(lastName);
            user.setPassword(password);
            user.setRole(role);
            user.setUsername(username);

            System.out.println("user found");


        }else {
            System.out.println("User not found: ");
        }
        result.close();
        statement.close();


        return user;

    }


}
