package View;

import Model.User;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class AdminView extends JFrame {
    private JLabel titleLabel;
    private JLabel idLabel;
    private JLabel usernameLabel;
    private JLabel firstNameLabel;
    private JLabel lastNameLabel;
    private JLabel emailLabel;
    private JLabel passwordLabel;
    private JTextField usernameField;
    private JTextField idField;
    private JTextField firstNameField;
    private JTextField lastNameField;
    private JTextField emailField;
    private JTextField passwordField;
    private JCheckBox AdminCheckbox;
    private JComboBox<String> comboBox;
    private JButton addUserButton;
    private JButton deleteUserButton;
    private JButton updateUserButton;
    private JButton viewAllUsersButton;
    private JTextArea textArea1;

    public AdminView() {

        setTitle("Admin Panel");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new GridLayout(10, 2));


        titleLabel = new JLabel("Admin ");
        add(titleLabel);
        add(new JLabel());

        idLabel = new JLabel("Id:");
        add(idLabel);
        idField = new JTextField(20);
        add(idField);

        firstNameLabel = new JLabel("Firstname:");
        add(firstNameLabel);
        firstNameField = new JTextField(20);
        add(firstNameField);

        lastNameLabel = new JLabel("Lastname:");
        add(lastNameLabel);
        lastNameField = new JTextField(20);
        add(lastNameField);

        emailLabel = new JLabel("Email:");
        add(emailLabel);
        emailField = new JTextField(20);
        add(emailField);

        usernameLabel = new JLabel("Username:");
        add(usernameLabel);
        usernameField = new JTextField(20);
        add(usernameField);


        passwordLabel = new JLabel("Password:");
        add(passwordLabel);
        passwordField = new JTextField(20);
        add(passwordField);

//        AdminCheckbox = new JCheckBox("Select the role");
//        add(AdminCheckbox);

        comboBox = new JComboBox<>();
        comboBox.addItem("Select the role");
        comboBox.addItem("Admin");
        comboBox.addItem("client");
        comboBox.addItem("organizer");
        add(comboBox);

        addUserButton = new JButton("Add User");
        add(addUserButton);
        deleteUserButton = new JButton("Delete User");
        add(deleteUserButton);

        updateUserButton = new JButton("Update User");
        add(updateUserButton);
        viewAllUsersButton = new JButton("View All Users");
        add(viewAllUsersButton);

        textArea1 = new JTextArea();
        add(textArea1);



        setSize(600, 400);
        setVisible(true);
    }

    // Getters for the form data
    public String getUsername() {
        return usernameField.getText();
    }
    public JTextArea getTextArea1() {
        return textArea1;
    }

    public String getPassword() {
        return passwordField.getText();
    }


    public JTextField getUsernameField() {
        return usernameField;
    }

    public JTextField getIdField() {
        return idField;
    }

    public JTextField getFirstNameField() {
        return firstNameField;
    }

    public JTextField getLastNameField() {
        return lastNameField;
    }

    public JTextField getEmailField() {
        return emailField;
    }

    public JTextField getPasswordField() {
        return passwordField;
    }

    public JCheckBox getAdminCheckbox() {
        return AdminCheckbox;
    }

    public JComboBox<String> getComboBox() {
        return comboBox;
    }

    public JButton getAddUserButton() {
        return addUserButton;
    }

    public JButton getDeleteUserButton() {
        return deleteUserButton;
    }

    public JButton getUpdateUserButton() {
        return updateUserButton;
    }

    public JButton getViewAllUsersButton() {
        return viewAllUsersButton;
    }
//    public static void main(String[] args) {
//        AdminView adminView = new AdminView();
//        //adminView.setVisible(true);
//    }

}






