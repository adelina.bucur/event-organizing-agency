package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ClientView extends JFrame {
    private JButton eventListButton;
    private JTextArea eventTable;

    private JCheckBox ScopeCheckbox;
    private JCheckBox LocationCheckbox;
    private JTextArea textArea1;
    public ClientView() {
        setTitle("Client");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new GridLayout(10, 2));

        eventListButton = new JButton("View Event List");
        add(eventListButton, BorderLayout.NORTH);

        eventTable = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(eventTable);
        add(scrollPane, BorderLayout.CENTER);

        LocationCheckbox = new JCheckBox("Filtred by Location");
        add(LocationCheckbox);

        ScopeCheckbox = new JCheckBox("Filtred by Scope");
        add(ScopeCheckbox);

        textArea1 = new JTextArea();
        add(textArea1);

        setSize(600, 400);
        setVisible(true);
    }




    public JButton getEventListButton() {
        return eventListButton;
    }
    public JTextArea getTextArea1() {
        return textArea1;
    }

//    public static void main(String[] args) {
//    new ClientView();
//     }
}
