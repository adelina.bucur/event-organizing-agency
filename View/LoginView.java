package View;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

public class LoginView extends JFrame {
    private JLabel titleLabel;
    private JLabel usernameLabel;
    private JLabel passwordLabel;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JButton loginButton;
    private JButton viewEventList;
    private JButton registerButton;
    private JTextArea textArea1;

    public LoginView() {
        // Set up the window
        setTitle("User Login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(10, 2));

        // Add the components to the window
        titleLabel = new JLabel("User Login");
        add(titleLabel);
        add(new JLabel());

        usernameLabel = new JLabel("Username:");
        add(usernameLabel);
        usernameField = new JTextField(20);
        add(usernameField);

        passwordLabel = new JLabel("Password:");
        add(passwordLabel);
        passwordField = new JPasswordField(20);
        add(passwordField);

        add(new JLabel());
        loginButton = new JButton("Login");
        add(loginButton);

        add(new JLabel());
        viewEventList = new JButton("View Event List");
        add(viewEventList);

        add(new JLabel());
        registerButton = new JButton("Register");
        add(registerButton);

        textArea1 = new JTextArea();
        add(textArea1);


        // Set the size and display the window
        setSize(800, 800);
        setVisible(true);
    }

    // Getters for the form data
    public String getUsername() {
        return usernameField.getText();
    }

    public String getPassword() {
        return new String(passwordField.getPassword());
    }
    public JTextArea getTextArea1() {
        return textArea1;
    }

    // Login button listener
    public void addLoginListener(ActionListener listener) {
        loginButton.addActionListener(listener);
    }

    public JButton getLoginButton() {
        return loginButton;
    }

    public JTextField getUsernameField() {
        return usernameField;
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public JButton getViewEventListButton() {
        return viewEventList;
    }

    public JButton getRegisterButton() {
        return registerButton;
    }
}