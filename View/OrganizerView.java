package View;

import Model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class OrganizerView extends JFrame {
    private JLabel titleLabel;
    private JLabel idUserLabel;
    private JLabel idLabel;
    private JLabel usernameLabel;
    private JLabel firstNameLabel;
    private JLabel lastNameLabel;
    private JLabel emailLabel;
    private JLabel passwordLabel;
    private JTextField usernameField;
    private JTextField idField;
    private JTextField idUserField;
    private JTextField firstNameField;
    private JTextField lastNameField;
    private JTextField emailField;
    private JTextField passwordField;
    private JTextField idUserFieldField;
    private JCheckBox AdminCheckbox;
    private JComboBox<String> comboBox;
    private JButton addUserButton;
    private JButton deleteUserButton;
    private JButton updateUserButton;
    private JButton viewAllUsersButton;
    private JTable usersTable;
    private JTextField nameField;
    private JTextField dateField;
    private JTextField locationField;
    private JTextField nrPersonsField;
    private JTextField scopeField;
    private JButton addEventButton;
    private JButton viewEventsButton;
    private JTextArea Table;
    private JCheckBox ScopeCheckbox;
    private JCheckBox LocationCheckbox;
    private JCheckBox NrpersonsCheckbox;
    private JTextArea textArea1;

    public OrganizerView() {

        setTitle("Organizer Panel");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new GridLayout(20, 6));


        titleLabel = new JLabel("Admin ");
        add(titleLabel);
        add(new JLabel());

        idLabel = new JLabel("Id:");
        add(idLabel);
        idField = new JTextField(20);
        add(idField);

        firstNameLabel = new JLabel("Firstname:");
        add(firstNameLabel);
        firstNameField = new JTextField(20);
        add(firstNameField);

        lastNameLabel = new JLabel("Lastname:");
        add(lastNameLabel);
        lastNameField = new JTextField(20);
        add(lastNameField);

        emailLabel = new JLabel("Username:");
        add(emailLabel);
        emailField = new JTextField(20);
        add(emailField);

        usernameLabel = new JLabel("Username:");
        add(usernameLabel);
        usernameField = new JTextField(20);
        add(usernameField);


        passwordLabel = new JLabel("Password:");
        add(passwordLabel);
        passwordField = new JTextField(20);
        add(passwordField);

        comboBox = new JComboBox<>();
        comboBox.addItem("Select the role");
        comboBox.addItem("Admin");
        comboBox.addItem("Client");
        comboBox.addItem("Organizer");
        add(comboBox);

        addUserButton = new JButton("Add User");
        add(addUserButton);
        deleteUserButton = new JButton("Delete User");
        add(deleteUserButton);

        updateUserButton = new JButton("Update User");
        add(updateUserButton);
        viewAllUsersButton = new JButton("View All Users");
        add(viewAllUsersButton);


        Table = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(Table);
        add(scrollPane, BorderLayout.CENTER);

        idUserLabel = new JLabel("Id:");
        add(idUserLabel);
        idUserField = new JTextField(20);
        add(idUserField);

        JLabel nameLabel = new JLabel("Event Name:");
        add(nameLabel);
        nameField = new JTextField();
        add(nameField);

        JLabel dateLabel = new JLabel("Date:");
        add(dateLabel);
        dateField = new JTextField();
        add(dateField);

        JLabel locationLabel = new JLabel("Location:");
        add(locationLabel);
        locationField = new JTextField();
        add(locationField);

        JLabel nrPersonsLabel = new JLabel("Number of Persons:");
        add(nrPersonsLabel);
        nrPersonsField = new JTextField();
        add(nrPersonsField);

        JLabel scopeLabel = new JLabel("Scope:");
        add(scopeLabel);
        scopeField = new JTextField();
        add(scopeField);

        addEventButton = new JButton("Add Event");
        add(addEventButton);

        viewEventsButton = new JButton("View Event List");
        add(viewEventsButton);

        LocationCheckbox = new JCheckBox("Filtred by Location");
        add(LocationCheckbox);

        ScopeCheckbox = new JCheckBox("Filtred by Scope");
        add(ScopeCheckbox);

        NrpersonsCheckbox = new JCheckBox("Filtred by Nr persons");
        add(NrpersonsCheckbox);

        textArea1 = new JTextArea();
        add(textArea1);


        setSize(800, 800);
        setVisible(true);
    }

    public String getUsername() {
        return usernameField.getText();
    }

    public String getPassword() {
        return passwordField.getText();
    }
    public JTextArea getTextArea1() {
        return textArea1;
    }

    public void addAddUserListener(ActionListener listener) {
        addUserButton.addActionListener(listener);
    }

    public void addDeleteUserListener(ActionListener listener) {
        deleteUserButton.addActionListener(listener);
    }

    public void addUpdateUserListener(ActionListener listener) {
        updateUserButton.addActionListener(listener);
    }

    public void addViewAllUsersListener(ActionListener listener) {
        viewAllUsersButton.addActionListener(listener);
    }

    // Table methods
    public JTable getUsersTable() {
        return usersTable;
    }

    public JLabel getTitleLabel() {
        return titleLabel;
    }

    public JButton getAddUserButton() {
        return addUserButton;
    }

    public JLabel getIdUserLabel() {
        return idUserLabel;
    }

    public JTextField getUsernameField() {
        return usernameField;
    }

    public JTextField getIdField() {
        return idField;
    }

    public JTextField getIdUserField() {
        return idUserField;
    }

    public JTextField getFirstNameField() {
        return firstNameField;
    }

    public JTextField getLastNameField() {
        return lastNameField;
    }

    public JTextField getEmailField() {
        return emailField;
    }

    public JTextField getPasswordField() {
        return passwordField;
    }

    public JTextField getIdUserFieldField() {
        return idUserFieldField;
    }

    public JCheckBox getAdminCheckbox() {
        return AdminCheckbox;
    }

    public JComboBox<String> getComboBox() {
        return comboBox;
    }

    public JTextField getNameField() {
        return nameField;
    }

    public JTextField getDateField() {
        return dateField;
    }

    public JTextField getLocationField() {
        return locationField;
    }

    public JTextField getNrPersonsField() {
        return nrPersonsField;
    }

    public JTextField getScopeField() {
        return scopeField;
    }

    public JTextArea getTable() {
        return Table;
    }

    public JCheckBox getScopeCheckbox() {
        return ScopeCheckbox;
    }

    public JCheckBox getLocationCheckbox() {
        return LocationCheckbox;
    }

    public JCheckBox getNrpersonsCheckbox() {
        return NrpersonsCheckbox;
    }

    public JButton getDeleteUserButton() {
        return deleteUserButton;
    }

    public JButton getUpdateUserButton() {
        return updateUserButton;
    }

    public JButton getViewAllUsersButton() {
        return viewAllUsersButton;
    }

    public JButton getAddEventButton() {
        return addEventButton;
    }

    public JButton getViewEventsButton() {
        return viewEventsButton;
    }

    public int getSelectedRow() {
        return usersTable.getSelectedRow();
    }
    public OrganizerView(User user) {
    }

//    public static void main(String[] args) {
//        OrganizerView organizerView = new OrganizerView ();
//
//    }

}

