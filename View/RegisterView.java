package View;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

public class RegisterView extends JFrame {
    private JLabel titleLabel;
    private JLabel usernameLabel;
    private JLabel passwordLabel;
    private JLabel confirmPasswordLabel;
    private JLabel emailLabel;
    private JLabel firstNameLabel;
    private JLabel lastNameLabel;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JTextField firstNameField;
    private JTextField lastNameField;

    private JTextField emailField;
    private JComboBox<String> comboBox;
    private JButton registerButton;

    public RegisterView() {
        // Set up the window
        setTitle("User Registration");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new GridLayout(10, 2));

        // Add the components to the window
        titleLabel = new JLabel("User Registration");
        add(titleLabel);
        add(new JLabel());

        firstNameLabel = new JLabel("Firstname:");
        add(firstNameLabel);
        firstNameField = new JTextField(20);
        add(firstNameField);

        lastNameLabel = new JLabel("Lastname:");
        add(lastNameLabel);
        lastNameField = new JTextField(20);
        add(lastNameField);

        usernameLabel = new JLabel("Username:");
        add(usernameLabel);
        usernameField = new JTextField(20);
        add(usernameField);

        passwordLabel = new JLabel("Password:");
        add(passwordLabel);
        passwordField = new JPasswordField(20);
        add(passwordField);

        emailLabel = new JLabel("Email:");
        add(emailLabel);
        emailField = new JTextField(20);
        add(emailField);

        comboBox = new JComboBox<>();
        comboBox.addItem("Select the role");
        comboBox.addItem("Admin");
        comboBox.addItem("Client");
        comboBox.addItem("Organizer");
        add(comboBox);

        registerButton = new JButton("Register");
        add(new JLabel());
        add(registerButton);

        // Set the size and display the window
        setSize(400, 300);
        setVisible(true);
    }

    // Getters for the form data
    public String getUsername() {
        return usernameField.getText();
    }

    public String getPassword() {
        return new String(passwordField.getPassword());
    }



    public String getEmail() {
        return emailField.getText();
    }

    // Register button listener
    public void addRegisterListener(ActionListener listener) {
        registerButton.addActionListener(listener);
    }

    public JButton getRegisterButton() {
        return registerButton;
    }

    public JTextField getUsernameField() {
        return usernameField;
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public JTextField getFirstNameField() {
        return firstNameField;
    }

    public JTextField getLastNameField() {
        return lastNameField;
    }

    public JTextField getEmailField() {
        return emailField;
    }

    public JComboBox<String> getComboBox() {
        return comboBox;
    }

    public static void main(String[] args) {
        RegisterView registerView = new RegisterView();
       // registerView .setVisible(true);
   }
}